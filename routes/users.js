const { Router } = require('express');
const { getUsers, createUser, deleteUser, updateUser, getUserById ,getUserByName} = require('../controllers/users');

const router = Router();

router.get("/", getUsers)

router.post("/create", createUser)

router.delete("/delete/:id", deleteUser)

router.put("/update/:id", updateUser)

router.get("/get/:id", getUserById)

router.get("/name/:name",getUserByName)

module.exports = router;