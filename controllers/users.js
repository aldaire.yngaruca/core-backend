const { response } = require('express');
const User = require("../models/User");

const getUsers = async (req, res = response) => {

    try {
        const users = await User.find()

        res.json({
            ok: true,
            data: users
        })
    } catch (e) {
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

const createUser = async (req, res) => {

    try {
        const user = new User(req.body);
        await user.save();

        res.status(201).json({
            ok: true,
            user: user.name
        })
    } catch (e) {
        console.log(e)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });

    }

}


const deleteUser = async (req, res) => {
    const userId = req.params.id;
    const uid = req.uid;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no usuario por ese id'
            });
        }

        await User.findByIdAndDelete(userId);

        res.json({
            ok: true,
            msg: "User Deleted"
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const updateUser = async (req, res) => {
    const userId = req.params.id;

    try {

        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no existe con ese id'
            });
        }

        const newUser = {
            ...req.body,
            _id: userId
        }

        const userUpdated = await User.findByIdAndUpdate(userId, newUser, { new: true });

        res.json({
            ok: true,
            user: userUpdated
        });


    } catch (e) {
        console.log(e);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getUserById = async (req, res) => {
    const userId = req.params.id;
    try {

        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no existe con ese id'
            });
        }


       return  res.json({
            ok: true,
            data: user
        });


    } catch (e) {
        console.log(e);
       return  res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getUserByName = async (req, res) => {
    const userName = req.params.name

    try {

        const regex = new RegExp(userName, 'i'); 
        const user = await User.find({name:regex})        
        
        if(user.length === 0){
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no existe con ese nombre'
            });
        }

         res.json({
            ok: true,
            data: user
        });

    }catch(e){
        console.log(e);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = { getUsers, createUser, deleteUser, updateUser, getUserById ,getUserByName}